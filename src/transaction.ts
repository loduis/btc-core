/// <reference path="../index.d.ts" />

const COINBASE = 'coinbase';

export class Transaction {
    public data: any;
    private client: Blockchain;
    private hash?: string;
    private block?: number;
    constructor (client: Blockchain, hash?: string, block?: number) {
        this.client = client;
        this.hash = hash;
        this.block = block;
    }
    async get (hash?: string) {
        const tx = this.data || await this.raw(hash);
        if (!this.block) {
            this.block = (await this.client.block(tx.blockhash)).height;
        }
        this.hash = hash = (hash || tx.hash) as string;
        const outputs: Array<any> = [];
        const inputs = await this.getInputs(tx);
        const { time } = tx;
        for (let entry of tx.vout) {
            let { value } = entry;
            let addresses = this.getAddresses(entry);
            for (let address of addresses) {
                this.push(outputs, hash, address, value, time);
            }
        }
        return inputs.concat(outputs);
    }

    private async getInputs (tx: any): Promise<any> {
        const inputs: Array<any> = [];
        let { time, hash } = tx;
        const vouts: any = {};
        const others: Array<any> = [];
        for (let entry of tx.vin) {
            if (entry.hasOwnProperty(COINBASE)) {
                let { value } = tx.vout[0]; // ojo con eso
                this.push(inputs, hash, COINBASE, -value, time);
            } else if (entry.hasOwnProperty('txid')) {
                const { vout, txid } = entry;
                vouts[txid] = vout;
                others.push([txid, true]);
            } else {
                throw new Error('Invalid from');
            }
        }
        if (others.length) {
            const entries = await this.client.batch('getrawtransaction', ...others);
            for (let input of entries) {
                let txid = input.hash;
                let vout = vouts[txid];
                const output = input.vout[vout];
                const { value } = output;
                const addresses = this.getAddresses(output);
                for (let address of addresses) {
                    this.push(inputs, hash, address, -value, time);
                }
            }
        }
        return inputs;
    }

    private getAddresses(entry: any) {
        const { scriptPubKey } = entry;
        let addresses = scriptPubKey.addresses || [scriptPubKey.type];
        if (scriptPubKey.type == 'multisig' && addresses.length > 1) { // revisar https://live.blockcypher.com/btc/tx/60a20bd93aa49ab4b28d514ec10b06e1829ce6818ec06cd3aabd013ebcdc4bb1/ es un ejemplo el resulve la de destino
            addresses = [scriptPubKey.type];
        }
        if (!addresses.length || addresses.length > 1) {
            console.log(entry);
            throw new Error('REVISAR DIRECCIONES');
        }
        return addresses;
    }

    private async raw (hash?: string) {
        return await this.client.getRawTransaction((hash || this.hash) as string, true);
    }

    private push (array: Array<any>, hash: string, address: string, value: number, time: number) {
        array.push({
            address,
            value,
            time,
            block: this.block,
            hash
        });
    }
}
