/// <reference path="../index.d.ts" />

import { Transaction } from './transaction';

export class Block {
    protected client: Blockchain;

    constructor (client: Blockchain) {
        this.client = client;
    }

    async get (indexOrHash: number | string) {
        const result = await this.client.block(indexOrHash);
        const { tx, height } = result;
        delete result.tx;
        const self = this;
        const generator = async function* () {
            const params: Array<any> = [];
            for (let hash of tx) {
                params.push([hash, true]);
            }
            const data = await self.client.batch('getrawtransaction', ...params); // get all transactions
            const txs: Array<any> = []
            for (let entry of data) {
                let tx = new Transaction(self.client, entry.hash, height);
                tx.data = entry;
                txs.push(tx)
            }
            for (let tx of txs) {
                yield* await tx.get();
            }
        }
        Object.defineProperty(result, 'tx', {
            get: function () {
                return generator();
            }
        });
        return result;
    }

    async transactions(indexOrHash: number | string) {
        return (await this.get(indexOrHash)).tx;
    }

    async hash (index: number) {
        return this.client.blockHash(index);
    }
}
