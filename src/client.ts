import axios, {AxiosInstance } from 'axios';
import http from 'http';
const RPC_VERSION = '2.0';

export class Client {
    private http: AxiosInstance;

    constructor (username: string, password: string, hostname: string = '127.0.0.1', port: number = 8332) {
        const config = {
            baseURL: `http://${hostname}:${port}`,
            auth: {
                username,
                password
            },
            httpAgent: new http.Agent({ keepAlive: true }),
        };
        this.http = axios.create(config);
    }

    async getRawTransaction(hash: string, own: Boolean = false): Promise<any> {}

    async getBlock(indexOrHash: number | string): Promise<any> {}

    async getBlockHash(index: number): Promise<any> {}

    async getBlockCount(): Promise<any> {}

    async command(method: string, ...params: Array<any>) {
        return (await this.request(createParams(method, params))).result;
    }

    async* batch (method: string, params: Array<any>) {
        const args: Array<any> = []
        for (let entry of params) {
            args.push(createParams(method, entry));
        }
        const data = await this.request(args);
        for (let entry of data) {
            if (entry.error) {
                throw entry.error;
            }
            yield entry.result;
        }
    }

    async request (params: any) {
        const { data } = await this.http.post('/', params);
        return data;
    }

    async rest (...params: Array<any>) {
        const { data } = await this.http.get('/rest/' + params.join('/') + '.json');
        return data;
    }
}

function createParams (method: string, params: any) {
    if (!Array.isArray(params)) {
        params = params === undefined ? [] : [params];
    }
    return {
        method,
        jsonrpc: RPC_VERSION,
        params
    };
}

for (let method of ['getRawTransaction', 'getBlock', 'getBlockHash', 'getBlockCount']) {
    (Client.prototype as any)[method] = async function (...params: Array<any>) {
        return this.command(method.toLowerCase(), ...params);
    };
}
