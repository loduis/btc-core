import { Client } from './client';

export class Blockchain extends Client {
    async lastBlock () {
        return this.getBlockCount();
    }

    async blockHash(index?: number) {
        if (index === undefined) {
            index = await this.lastBlock();
        }
        return this.getBlockHash(index as number);
    }

    async block (indexOrHash: number | string) {
        if (indexOrHash === undefined) {
            indexOrHash = await this.blockHash();
        } else if (typeof indexOrHash === 'number') {
            indexOrHash = await this.blockHash(indexOrHash);
        }
        return this.getBlock(indexOrHash);
    }

    async* txouts (...params: Array<Array<any>>) {
        let i = 0;
        for (let entry of await this.batch('gettxout', ...params)) {
            try {
                let [hash, vout] = params[i];
                entry.hash = hash;
                entry.vout = vout;
                yield entry;
                i ++
            } catch (e) {
                console.log(entry);
                console.log(i);
                console.log(params);
                throw e;
            }
        }
    }

    async* rawTransactions (...params: Array<Array<any>>) {
        for (let entry of await this.batch('getrawtransaction', ...params)) {
            yield entry;
        }
    }
}
