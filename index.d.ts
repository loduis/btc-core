interface Client {

}

interface Blockchain {
    block (indexOrHash: number | string): Promise<any>
    blockHash (index?: number): Promise<any>
    getRawTransaction (hash: string, own: Boolean): Promise<any>
    batch (method: string, ...params: Array<any>): Promise<any>
    rawTransactions (...params: Array<any>): Promise<any>
    txouts (...params: Array<any>): Promise<any>
}
