import { expect } from 'chai';
import { Blockchain } from '../src';

let client: Blockchain;

context('Blockchain', () => {
    beforeEach(function () {
        client = new Blockchain((process.env as any).RPC_USERNAME, (process.env as any).RPC_PASSWORD)
    });

    it ('should request the block hash by index.', async () => {
        const hash = await client.getBlockHash(1);
        expect(hash).eqls('00000000839a8e6886ab5951d76f411475428afc90947ee320161bbf18eb6048');
    });

    it ('should request the block info by index.', async () => {
        const block = await client.block(1);
        console.log(block.tx.includes('0e3e2357e806b6cdb1f70b54c3a3a17b6714ee1f0e68bebb44a74b1efd512098'));
    });

    it ('should request the block by hash.', async () => {
        const block = await client.block('00000000839a8e6886ab5951d76f411475428afc90947ee320161bbf18eb6048');
        console.log(block.tx.includes('0e3e2357e806b6cdb1f70b54c3a3a17b6714ee1f0e68bebb44a74b1efd512098'));
    })
});
